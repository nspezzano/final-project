Tuesday 1/12: Added experience points, level up system and strength and defense factors. Began adding sneaking mechanics.

Wednesday 1/13: Continued working on sneaking mechanics.

Thursday 1/14: Continued working on sneaking mechanics.

Friday 1/15: Started on attack items. Else not working on line 604.

Tuesday 1/19: Expanded game world, added more dialogue and bug fixes.

Wednesday, Thursday, Friday 1/19: Added more dialogue, fixed bugs and improved code.