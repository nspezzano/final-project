class Enemy:

    def __init__(self, enemyHealth, attack):
        self.__enemyHealth = enemyHealth
        self.__attack = attack

    def get_enemyHealth(self):
            return self.__enemyHealth

    def get_attack(self):
            return self.__attack
